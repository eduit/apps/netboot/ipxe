# ipxe-incl-EFI-Patch.tgz

iPXE mit Patch für EFI, zur Verfügung gestellt von Christian Schneider.

Wir nutzen das unten beschriebene `undionly.kpxe` in der Ansible-Rolle für den [TFTP-Server](https://gitlab.ethz.ch/ansible-let/linux-tasks).


- [iPXE Repo](https://github.com/ipxe/ipxe)
- [Patch für UEFI](https://github.com/ipxe/ipxe/pull/612)

Zum bauen brauchen wir unter Debian 10 folgendes.

- `apt install build-essential liblzma-dev lzma lzma-dev genisoimage isolinux git mtools syslinux libcap-dev -y`

## PCBIOS

In `ipxe-incl-EFI-Patch/src`:

- `make -j 8 EMBED=../../menue-4-let.ipxe`

Damit werden folgende Dateien gebaut:

``` bash
- bin/ipxe.dsk
- bin/ipxe.iso
- bin/ipxe.lkrn
- bin/ipxe.pxe
- bin/ipxe.usb
- bin/undionly.kpxe
```

Die EFI Binarys müssen alle manuell gesetzt werden. `cd ipxe/src` und los gehts ...

## ISO (ipxe.iso)

- `make -j 8 EMBED=/root/menue-4-let.ipxe bin/ipxe.iso`
  * mit EMBED gibt man das ipxe File an das mit eingebaut werden soll

## PCBIOS (undionly.kpxe)

- `make -j 8 EMBED=../../menue-4-let.ipxe bin/undionly.kpxe`

## EFI (ipxe.efi)

- `make -j 8 EMBED=../../menue-4-let.ipxe bin-x86_64-efi/ipxe.efi`
- `make -j 8 EMBED=../../menue-4-let.ipxe bin-x86_64-efi/ipxe.usb`

# Notes

Wenn ein Preseed oder Kickstart File anhand der MAC-Adresse geladen werden soll, dann sollte die iPXE Variable so geschrieben werden. Standard sind Doppelpunkte.

- ${net0/mac:hexhyp}.ks ...
  - das ergibt 52-54-00-20-55-49.ks

---

# Links

- [Examples von Robin](https://gist.github.com/robinsmidsrod/2234639)

---

# menue-4-let.ipxe

Das ist ein kommentiertes Beispiel incl. Architecture Erkennung und EFI/PCBIOS Erkennung. Es muss eigentlich nur die <URL> angepasst werden zu einem Webserver.

- `ipxe_url http://<URL>`

Das iPXE-Script `menue-4-let.ipxe` wird wie oben beschrieben mit `EMBED` in das binary gebaut und kann dann via Menü verschiedenes nachladen.

---

# Debian

Bei den Debian Beispielen muss die Zeile `set preseed_url http://<URL>` angepasst werden.

- `static.ipxe`
- `dynamic.ipxe`

# Almalinux

Bei den Almaliinux Beispielen muss die Zeile `set kickstart_url http://<URL>/almalinux/${alma-linux-version}/${alma-flavor}` angepasst werden.


